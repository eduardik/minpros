const height = require('./tailwind.h_w.confg')
const width = height

module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  content: [],
  theme: {

    borderWidth:{
      default:'1px',
    },
    screens: {
      sm: '640px',
      md: '768px',
      lg: '1024px',
      xl: '1280px',
      tablet: '640px',
      laptop: '1024px',
      desktop: '1280px',
    },
    extend: {
      zIndex:{
        '45':'45',
        '41':'41',
        '60':'60'
      },
      backgroundSize: {
        'auto': 'auto',
        'cover': 'cover',
        'contain': 'contain',
        '50%': '50%',
        '16': '4rem',
      },
      fontFamily:{
        qwer:'Open Sans',
        source:'Source Sans Pro'
      },
      spacing:{
        '13':'3.25rem',
        '3.5':'0.870rem',
        '31':'7.8rem',
        '21':'5.25rem',
        '22':'5.5rem',
        '23':'5.75rem',
        '25':'6.25rem',
        '26':'6.5rem',
        '27':'6.75rem',
        '29':'7.25rem',
        '30':'7.45rem'

      },
      backgroundImage:{
        'mainPhoto':"url('/src/assets/mainPhoto.png')",
        'searchIcon':"url('/src/assets/serachIcon.svg')"
      },

      minWidth: {
        ...height,
        0: '0',
        '1/4': '25%',
        '1/2': '50%',
        '1/3': '33.33333333%',
        '2/3': '66.66666666%',
        '3/4': '75%',
        full: '100%'
      },
      maxWidth: {
        ...height,
        0: '0',
        '1/4': '25%',
        '1/2': '50%',
        '1/3': '33.33333333%',
        '2/3': '66.66666666%',
        '3/4': '75%',
        full: '100%'
      },
      minHeight: {
        ...height,
        0: '0',
        '1/4': '25%',
        '1/2': '50%',
        '1/3': '33.33333333%',
        '2/3': '66.66666666%',
        '3/4': '75%',
        full: '100%'
      },
      maxHeight: {
        ...height,
        0: '0',
        '1/4': '25%',
        '1/2': '50%',
        '1/3': '33.33333333%',
        '2/3': '66.66666666%',
        '3/4': '75%',
        full: '100%'
      },
      colors:{
        backGray: '#E5E5E5',
        darkerGreen: '#132F28',
        lightWhite: '#FFFFFF',
        darkGreen: '#124A3D',
        lightGreen: '#048D4B',
        white: '#EBEBEB',
         gray:{
           50:  '#FBFBFB',
           100: '#FFFFFF',
           200: '#EFF3F1',
           300: '#7C8785',
           400: '#565C59',
           500: '#3E403E',
           600: '#212220 '
         }
      },
      fontSize:{
        'base': ['16px', { lineHeight: '20px' }],
        'title':['32px', {lineHeight: '44px'}],
        'news-card':['20px',{lineHeight: '27.24px'}],
        'par':['18px',{lineHeight: '26px'}],
        'logoh1':['22px', {lineHeight: '30px'}],
        'newsTitle':['28px', {lineHeight: '38.13px'}],
        'news2Title':['24px', {lineHeight: '32.68px'}],
      },
      height,
      width


    },
    transitionProperty:{
      'backgroundPosition' : 'background-position',
    },
    backgroundSize:{
      '200%-100%':'200% 100%',
      '100%':'100%'

    },
    backgroundPosition:{
      '16px': '16px',
      '100% 0': '-100% 0',
      'right': 'center right',
      'center':'center'
    },
  },
  plugins: [],
}
